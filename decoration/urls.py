from django.urls import path
from . import views
from decoration import views

urlpatterns = [
    path('', views.HomeTemplateView.as_view(), name='homepage'),
    path('decoration/<int:pk>/', views.DecorationDetailView.as_view(), name='decoration'), # ca sa stie care dintre ele obiecte sa il afiseze
    path('category/<int:pk>/', views.CategoryDetailView.as_view(), name='category_detail'), # ca sa stie care dintre ele obiecte sa il afiseze
    # path('festival_items/', views.festival_items, name='festival_items'),
    # # path('clothes/', views.clothes, name='clothes'),
    # path('category/<int:pk>/', views.CategoryDetailView.as_view(), name='category'),
]
