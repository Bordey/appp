from django.shortcuts import render
from django.views.generic import DetailView, TemplateView

from decoration.models import Product, Category



def clothes(request):
    return render(request, 'category/clothes.html')

def decorations(request):
    return render(request, 'category/decorations.html')

def festival_items(request):
    return render(request, 'category/festival_items.html')



class HomeTemplateView(TemplateView):
    template_name = 'homepage/home.html'


class DecorationTemplateView(TemplateView):
    template_name = 'decoration/decorations.html'


class DecorationDetailView(DetailView):
    template_name = 'decoration/decorations.html'  # calea spre html file, excludem mereu templates
    model = Product


# class CategoryTemplateView(TemplateView):
#     template_name = 'category/category_detail.html'


class CategoryDetailView(DetailView):
    template_name = 'category/category.html'  # calea spre html file, excludem mereu templates
    model = Category


